import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';
import {
	Container,
	Header,
	Left,
	Body,
	Right,
	Text,
	Button,
} from 'native-base'
import Icon from 'react-native-vector-icons/Ionicons'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import DummyCallout from './DummyCallout'
import { characters } from './Data';

var { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = -6.583078;
const LONGITUDE = 106.813140;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

class Map extends Component {

  state = {
    showGoodOnly: false,
  }

  render() {
    const { navigate, goBack } = this.props.navigation
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left style={styles.backHeader}>
            <Button transparent onPress={() => goBack()}>
              <Icon name="ios-arrow-back" size={25} color="#000000" />
              <Text style={styles.back}>Back</Text>
            </Button>
          </Left>
          <Body>
            <Text style={styles.title}>SALES LOCATION</Text>
          </Body>
          <Right />
        </Header>
        <Container style={styles.containerMap}>
          <MapView
            style={styles.map}
            initialRegion = {{
              latitude: LATITUDE,
              longitude: LONGITUDE,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            }}>
            {characters.map((character, index) =>
              this.state.showGoodOnly && !character.good ||
              <MapView.Marker
                coordinate={{
                  latitude: character.coordinate[0],
                  longitude: character.coordinate[1],
                }}
                calloutOffset={{ x: -8, y: 28 }}
                pinColor={character.good ? '#f44336' : '#f44336'}
                key={index}>
                <MapView.Callout tooltip style={styles.callout}>
                 <DummyCallout
                   name={character.name}
                   image={character.image}/>
                </MapView.Callout>
              </MapView.Marker>
            )}
          </MapView>
          <View style={styles.buttonContainer}>
            <View style={styles.bubble}>
              <Text>Tap on markers to see sales information</Text>
            </View>
          </View>
        </Container>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
		backgroundColor: '#ffffff'
	},
  containerMap: {
    position: 'absolute',
    top: 100,
    left: 20,
    right: 20,
    height: '50%',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: 'transparent',
  },
  bubble: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,0.7)',
    borderRadius: 20,
    padding: 12,
    width: 160,
  },
  callout: {
    width: 140,
  },
  header: {
		height: 70
	},
	backHeader: {
		flexDirection: 'row'
	},
	back: {
		fontSize: 18,
		color: '#000000'
	},
	title: {
		fontWeight: 'bold'
	}
});

export default Map
