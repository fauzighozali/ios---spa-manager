import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import {
	StyleSheet,
	Alert,
	AsyncStorage,
	TouchableHighlight,
	ReactNative,
	PropTypes,
	View,
	Dimensions,
	TouchableOpacity,
	Image
} from 'react-native'
import {
	Container,
	Header,
	Left,
	Body,
	Right,
	Text,
	Button,
	Content,
	List,
	ListItem
} from 'native-base'
import Icon from 'react-native-vector-icons/Ionicons'
import MapView, { PROVIDER_GOOGLE, Callout, Marker } from 'react-native-maps'

import PriceMarker from './PriceMarker'
import CustomCallout from './CustomCallout'

var { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = -6.583078;
const LONGITUDE = 106.813140;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

class Location extends Component {

		state = {
				region: {
					latitude: LATITUDE,
					longitude: LONGITUDE,
					latitudeDelta: LATITUDE_DELTA,
					longitudeDelta: LONGITUDE_DELTA,
				},
				markers: [
					{
						coordinate: {
							latitude: LATITUDE,
							longitude: LONGITUDE,
						},
					},
				],
		}

	render() {
		const { navigate, goBack } = this.props.navigation
		const { params } = this.props
		return (
			<Container style={styles.container}>

				<Header style={styles.header}>
					<Left style={styles.backHeader}>
						<Button transparent onPress={() => goBack()}>
							<Icon name="ios-arrow-back" size={25} color="#000000" />
							<Text style={styles.back}>Back</Text>
						</Button>
					</Left>
					<Body>
						<Text style={styles.title}>SALES LOCATION</Text>
					</Body>
					<Right />
				</Header>

				<Container style={styles.containerMap}>
				<MapView
					style={styles.map}
					initialRegion={this.state.region}>
					<MapView.Marker
						ref="m1"
						coordinate={this.state.markers[0].coordinate}>
						<MapView.Callout>
							<View>
								<Text>Fauzi Ghozali</Text>
							</View>
						</MapView.Callout>
					</MapView.Marker>
				</MapView>
				<View style={styles.buttonContainer}>
					<View style={styles.bubble}>
						<Text>Tap on markers to see sales information</Text>
					</View>
				</View>
				</Container>

			</Container>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#ffffff'
	},
	containerMap: {
		position: 'absolute',
	 	top: 100,
	 	left: 20,
	 	right: 20,
		height: '50%',
	 	alignItems: 'center',
	},
	map: {
	 position: 'absolute',
	 top: 0,
	 left: 0,
	 right: 0,
	 bottom: 0,
 },
 bubble: {
	 flex: 1,
	 backgroundColor: 'rgba(255,255,255,0.7)',
	 paddingHorizontal: 18,
	 paddingVertical: 12,
	 borderRadius: 20,
 },
 latlng: {
	 width: 200,
	 alignItems: 'stretch',
 },
 button: {
	 width: 80,
	 paddingHorizontal: 12,
	 alignItems: 'center',
	 marginHorizontal: 10,
 },
 buttonContainer: {
	 flexDirection: 'row',
	 marginVertical: 20,
	 marginLeft: 20,
	 marginRight: 20,
	 backgroundColor: 'transparent',
 },
	header: {
		height: 70
	},
	backHeader: {
		flexDirection: 'row'
	},
	back: {
		fontSize: 18,
		color: '#000000'
	},
	title: {
		fontWeight: 'bold'
	}
})

export default Location
