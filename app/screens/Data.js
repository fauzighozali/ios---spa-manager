export const characters = [
  {
    name: 'Sales Satu',
    image: 'https://img.buzzfeed.com/buzzfeed-static/static/2016-12/8/10/asset/buzzfeed-prod-fastlane03/sub-buzz-3484-1481210659-5.png',
    coordinate: [ -6.583078, 106.813140 ],
    good: true,
  },
  {
    name: 'Sales Dua',
    image: 'https://img.buzzfeed.com/buzzfeed-static/static/2016-12/8/10/asset/buzzfeed-prod-fastlane02/sub-buzz-25788-1481210705-7.png',
    coordinate: [ -6.577771, 106.814406 ],
    good: true,
  },
  {
    name: 'Sales Tiga',
    image: 'https://img.buzzfeed.com/buzzfeed-static/static/2016-12/8/10/asset/buzzfeed-prod-fastlane01/sub-buzz-5211-1481210889-4.png',
    coordinate: [ -6.574808, 106.822109 ],
    good: true,
  },
];
